package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Dado dado = new Dado();
        Io io = new Io();
        Sorteio sorteio = new Sorteio();

        dado.setFaces(6);

        for (int j =0;j<3; j++){
            sorteio.zerarSoma();

            ArrayList<Integer> valorSorteado = new ArrayList<Integer>();
            for (int i = 0;i<3;i++){
                valorSorteado.add(sorteio.sortear(dado));
                sorteio.setSomaSorteio(valorSorteado.get(i));
            }
            for (int i = 0;i<3;i++){
                io.mostraMensagem("Sorteio "+ (i+1) + ": " + String.valueOf(valorSorteado.get(i)));
            }

            io.mostraMensagem("Soma dos sorteios: " + String.valueOf(sorteio.getSomaSorteio()));

        }

//        ArrayList<Integer> valorSorteado = new ArrayList<Integer>();
//        for (int i = 0;i<3;i++){
//            valorSorteado.add(sorteio.sortear(dado));
//            sorteio.setSomaSorteio(valorSorteado.get(i));
//        }
//        for (int i = 0;i<3;i++){
//            io.mostraMensagem("Sorteio "+ (i+1) + ": " + String.valueOf(valorSorteado.get(i)));
//        }
//
//        io.mostraMensagem("Soma dos sorteios: " + String.valueOf(sorteio.getSomaSorteio()));

    }
}
