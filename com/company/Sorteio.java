package com.company;

import java.util.Random;

public class Sorteio {
    private int somaSorteio;

    public Sorteio() {
        somaSorteio = 0;
    }

    public int sortear(Dado dado){
        Random random   = new Random();
        return (random.nextInt(dado.getFaces())+1);
    }

    public void zerarSoma(){
        somaSorteio = 0;
    }
    public int getSomaSorteio() {
        return somaSorteio;
    }

    public void setSomaSorteio(int somaSorteio) {
        this.somaSorteio = this.somaSorteio +somaSorteio;
    }
}
