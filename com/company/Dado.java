package com.company;

public class Dado {
    private int faces;
    //private ArrayList<Integer> numFace;

    public Dado(int faces) {
        this.faces = faces;
    }

    public Dado() {

    }

    public int getFaces() {
        return faces;
    }

    public void setFaces(int faces) {
        this.faces = faces;
    }
}
